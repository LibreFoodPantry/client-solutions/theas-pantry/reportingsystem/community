# Thea's Pantry Worcester County Food Bank Report

## Current System

### Data

- Date of visit
- ID number
- Resident/Commuter
- Zipcode
- First visit yes/no
- Number of people in household 0/1/2/3/4/5/5+
- Age range 0-4/5-17/18-64/64+
- Employed/Unemployed
- SS, TANF/EADC, SNAP/Food stamps, WIC, SFSP, School Breakfast, School Lunch, Financial Aid, Other, None
- Would like assistance applying for SNAP
- Pounds taken

### Example Report

![example report](November2020TheasPantryReport.png)

### Report Generation (guess)

This guess is based on the example report and what we know about the data kept. It may be wrong - we need confirmation/correction from the customer.

1. **New Since** is for a half-year (1 Jan-30 June or 1 July-31-Dec). This is based on the example report and the fact that example report is supposed to be for November. Collect all records back to beginning of 1/2 year.
    1. Records are grouped by zipcode.
        - **Households** -  Count the number of records that are marked as first visit
        - **People** - Sum of (households *  number of people in household)
2. **Remainder of the report** is generated for a single month. Collect all records since the first day of the month.
    1. Records are grouped by zipcode.
    2. **Month Total**
        - **Households** -  Count the number of records *(count every visit, not unique households?)*
        - **People** - Sum of (households *  number of people in household) *(or count of visits?)*
        - **Total visits** - which column is this totaling? People?
    3. **Ages Served**
        - This is ages of the people who filled out the form, since the data only contains the age of the person with the ID, not for every household member.
    4. **Primary Household Income Source**
        - This is count of boxes checked for Employed, Unemployed, SS, TANF/EADC
        - **Other** contains a labeled count of boxes checked for Financial Aid or Other
    5. **Participation in Fed Programs**
        - This is a count of boxes checked for Food Stamps, WIC, School Breakfast, School Lunch, SFSP

## New System

### Data

#### Guest Information

- Date/time of information entry/update
- ID number
- Resident/Commuter
- Zipcode
- First visit yes/no
- Number of people in household 0/1/2/3/4/5/5+
- Age range 0-4/5-17/18-64/64+
- Employed/Unemployed
- SS, TANF/EADC, SNAP/Foodstamps, WIC, SFSP, School Breakfast, School Lunch, Financial Aid, Other, None
- Would like assistance applying for SNAP

#### Inventory Checkout

- Date/time of checkout
- ID number
- Pounds taken

### Report Generation (proposed)

1. Collect all Inventory Checkout records for the calendar month.
2. Match them (by ID) with the most recent guest information record for that ID, and make a new record with all the information (like the current system's records)
3. Follow the same steps as report generation for the current system.

### Assumptions/Questions
